const express = require('express')
const app = express()//server.add(mount)
const PORT = process.env.PORT || 5000

app.use(express.json())


app.get('/', (req, res) => {
  res.json("Welcome")
})


app.listen(PORT, () => {
  console.log("Server running")
})